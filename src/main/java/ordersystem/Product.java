package ordersystem;
/*
    Copyright (C) 2018 Karl R. Wurst

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents a Product with a SKU number, unit price, and description.
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
public class Product {
	
	private long sku;
	private double unitPrice;
	private String description;
	
	/**
	 * Represents a Product with a SKU number, unit price, and description.
	 * 
	 * @param unitPrice must be non-negative
	 * @param description
	 */
	public Product(double unitPrice, String description) {
		if (unitPrice < 0) {
			throw new IllegalArgumentException("Unit price must be non-negative.");
		}
		this.unitPrice = unitPrice;
		this.description = description;
	}

	/**
	 * Sets the SKU number
	 * @param sku
	 */
	public void setSku(long sku) {
		this.sku = sku;
	}
	
	/**
	 * Returns the SKU number
	 * @return the SKU number
	 */
	public long getSku() {
		return sku;
	}
	
	/**
	 * Returns the unit price
	 * @return unit price
	 */
	public double getUnitPrice() {
		return unitPrice;
	}

	/**
	 * Sets the unit price
	 * @param unitPrice must be non-negative
	 */
	public void setUnitPrice(double unitPrice) {
		if (unitPrice < 0) {
			throw new IllegalArgumentException("Unit price must be non-negative.");
		}
		this.unitPrice = unitPrice;
	}

	/**
	 * Returns the description
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Product [sku=" + sku + ", unitPrice=" + unitPrice + ", description=" + description + "]";
	}

	/** 
	 * Products are equal *only* if their sku numbers are equal
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (sku != other.sku)
			return false;
		return true;
	}	

}